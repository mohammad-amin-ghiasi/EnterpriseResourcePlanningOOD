package database;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

/**
 * Created by jun on 5/25/16.
 * A simple LyAndroid Code.
 * ما را به جز خیالت
 * فکری دگر نباشد
 * در هیچ سر خیالی
 * زین خوب تر نباشد !‌
 */
public class MockedProjectsTable implements TableModel {
    private final static String[] NAMES = {"شماره", "نام پروژه", "تعداد ماژول", "تعداد کاربر", "تعداد توسعه دهندگان"};
    private final static Class[] CLASSES = {Integer.class, String.class, Integer.class, Integer.class, Integer.class};
    private final static int COLS = NAMES.length;
    private final static int ROWS = 5;
    private final static Object[][] DATAS;

    private static Object[] dataFactory(int id, String name, int modules, int users, int developers) {
        return new Object[]{new Integer(id), name, new Integer(modules), new Integer(users), new Integer(developers)};
    }

    static {
        DATAS = new Object[][]{
                dataFactory(1, "لیان", 5, 1, 17),
                dataFactory(2, "گوگل", 70, 5, 600),
                dataFactory(3, "فیس بوک", 7, 5, 1), //The only user is mike!
                dataFactory(4, "ضیظاک", 5, 1, 700),
                dataFactory(0, "نات اگزیست", 1000000, 7, 0),
        };
    }

    @Override
    public int getRowCount() {
        return ROWS;
    }

    @Override
    public int getColumnCount() {
        return COLS;
    }

    @Override
    public String getColumnName(int i) {
        return NAMES[i];
    }

    @Override
    public Class<?> getColumnClass(int i) {
        return CLASSES[i];
    }

    @Override
    public boolean isCellEditable(int i, int i1) {
        return false;
    }

    @Override
    public Object getValueAt(int i, int i1) {
        return DATAS[i][i1];
    }

    @Override
    public void setValueAt(Object o, int i, int i1) {
        DATAS[i][i1] = o;
    }

    @Override
    public void addTableModelListener(TableModelListener tableModelListener) {

    }

    @Override
    public void removeTableModelListener(TableModelListener tableModelListener) {

    }
}
