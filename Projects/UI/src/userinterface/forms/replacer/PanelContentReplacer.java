package userinterface.forms.replacer;

import userinterface.ReplaceableForm;

import javax.swing.*;

/**
 * Created by jun on 8/15/16.
 * A simple LyAndroid Code.
 */
public class PanelContentReplacer {
    private final static PanelContentReplacer INSTANCE = new PanelContentReplacer();

    private PanelContentReplacer() {
    }

    public static PanelContentReplacer getInstance() {
        return INSTANCE;
    }


    public void replaceView(JPanel jPanel, ReplaceableForm replaceableForm) {
        jPanel.removeAll();
        if (replaceableForm == null) {
            System.out.println("This is the error");
        } else if (replaceableForm.getView() == null) {
            System.out.println("Get vie is error");
        } else if(jPanel == null){
            System.out.println("kossher");
        }
        jPanel.add(replaceableForm.getView());
        jPanel.revalidate();
        jPanel.repaint();
    }
}
