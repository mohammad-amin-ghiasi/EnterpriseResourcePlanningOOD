package userinterface.forms;

import resource.ResourceTypes;
import userinterface.ReplaceableForm;
import userinterface.forms.replacer.PanelContentReplacer;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by jun on 8/15/16.
 * A simple LyAndroid Code.
 */
public class ResourceManagerUI implements ReplaceableForm {
    private final static ResourceManagerUI INSTANCE = new ResourceManagerUI();

    public static ResourceManagerUI getInstance() {
        return INSTANCE;
    }

    private ResourceManagerUI() {
        this.comboBox1.setModel(new DefaultComboBoxModel<>(ResourceTypes.values()));
        this.comboBox1.addActionListener(actionEvent -> {
            ResourceTypes selected = (ResourceTypes) comboBox1.getSelectedItem();
            PanelContentReplacer.getInstance().replaceView(mainFragment, selected.getUIManager());
        });
    }

    private JPanel mainPanel;
    private JPanel mainPane;
    private JComboBox<ResourceTypes> comboBox1;
    private JPanel mainFragment;


    @Override
    public JPanel getView() {
        return mainPanel;
    }
}
