package userinterface.forms.resource_type_manager_ui;

import userinterface.ReplaceableForm;

import javax.swing.*;

/**
 * Created by jun on 8/15/16.
 * A simple LyAndroid Code.
 */
public class AddHumanUI implements ReplaceableForm {
    private final static AddHumanUI INSTANCE = new AddHumanUI();

    private AddHumanUI() {
    }

    public static AddHumanUI getInstance() {
        return INSTANCE;
    }

    private JPanel panel1;

    @Override
    public JPanel getView() {
        System.out.println(panel1);
        return panel1;
    }
}
