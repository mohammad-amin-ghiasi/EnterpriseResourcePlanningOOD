package userinterface.forms;

import userinterface.ReplaceableForm;

import javax.swing.*;

/**
 * Created by jun on 5/25/16.
 * A simple LyAndroid Code.
 */
public class AssignRequirements implements ReplaceableForm {
    private final static AssignRequirements INSTANCE = new AssignRequirements();

    private AssignRequirements() {
    }

    public static AssignRequirements getInstance() {
        return INSTANCE;
    }

    private JPanel baseView;
    private JPanel contentPane;
    private JComboBox requirementType;
    private JTextField textField1;
    private JTextArea جزپیاتنیازمندیرااینجاTextArea;
    private JButton ok;
    private JButton افزودنوبستنButton;

    public static void main(String[] args) {
        AssignRequirements assignRequirements = new AssignRequirements();
        JFrame jFrame = new JFrame();
        jFrame.setContentPane(assignRequirements.getView());
        jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        jFrame.pack();
        ;
        jFrame.setVisible(true);
    }

    @Override
    public JPanel getView() {
        return baseView;
    }

    public JButton getOk() {
        return ok;
    }
}
