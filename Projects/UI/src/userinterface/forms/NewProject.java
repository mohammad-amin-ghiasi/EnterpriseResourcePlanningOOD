package userinterface.forms;

import userinterface.ReplaceableForm;

import javax.swing.*;

/**
 * Created by jun on 5/25/16.
 * A simple LyAndroid Code.
 */
public class NewProject implements ReplaceableForm {
    private final static NewProject INSTANCE = new NewProject();

    private NewProject() {
    }

    public static NewProject getInstance() {
        return INSTANCE;
    }

    private JPanel newProject;
    private JTextField projectName;
    private JLabel projectNameLabel;
    private JTextField modulesCount;
    private JLabel modulesCountLabel;
    private JLabel usersCountLabel;
    private JTextField usersCount;
    private JButton add;
    private JPanel baseView;

    @Override
    public JPanel getView() {
        return baseView;
    }

    public JButton getAdd() {
        return add;
    }
}
