package userinterface.forms;

import userinterface.ReplaceableForm;

import javax.swing.*;

/**
 * Created by jun on 5/24/16.
 * A simple LyAndroid Code.
 */
public class LoginForm implements ReplaceableForm {
    private final static LoginForm INSTANCE = new LoginForm();

    private LoginForm() {
    }

    public static LoginForm getInstance() {
        return INSTANCE;
    }

    private JTextField username;
    private JPasswordField password;
    private JButton loginButton;
    private JLabel usernameLable;
    private JLabel passwordLabel;
    private JPanel basePanel;
    private JPanel loginFormPane;

    @Override
    public JPanel getView() {
        return basePanel;
    }

    public JButton getLoginButton() {
        return loginButton;
    }

    private void createUIComponents() {
        // TODO: place custom component creation code here
    }
}
