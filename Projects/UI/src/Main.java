import userinterface.UIFrameChangerOnClickListener;
import userinterface.forms.*;

import javax.swing.*;

/**
 * Created by jun on 5/24/16.
 * A simple LyAndroid Code.
 */
public class Main extends JFrame {

    private Main() {
        super();
        this.setContentPane(LoginForm.getInstance().getView());
        LoginForm.getInstance().getLoginButton().addActionListener(
                new UIFrameChangerOnClickListener(this, MainPage.getInstance()));
        MainPage.getInstance().getNewProjectButton().addActionListener(
                new UIFrameChangerOnClickListener(this, NewProject.getInstance()));
        NewProject.getInstance().getAdd().addActionListener(
                new UIFrameChangerOnClickListener(this, AssignRequirements.getInstance()));
        MainPage.getInstance().getNewResourceButton().addActionListener(
                new UIFrameChangerOnClickListener(this, ResourceManagerUI.getInstance())
        );
        this.setExtendedState(MAXIMIZED_BOTH);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.pack();
        this.setVisible(true);
    }


    public static void main(String[] args) {
        new Main();
    }
}
