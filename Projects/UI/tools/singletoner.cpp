//In the name of GOD
//Created by Jun
//If you're not a genius, you can't understand this code 
//This code takes the name of a java class and creates singletone needs of this project

#include<iostream>

using namespace std; 
int main(int argv, char * args[]){
    string name; 
    if(argv>1){
	name = args[1];	
    } else {
	cin>>name;
    }
    cout<<"\tprivate final static "<<name<<" INSTANCE = new "<<name<<"();\n\n";
    cout<<"\tprivate "<<name<<"(){\n";
    cout<<"\t}\n\n";
    cout<<"\tpublic static "<<name<<" getInstance(){\n";
    cout<<"\t\treturn INSTANCE;\n";
    cout<<"\t}\n";
    return 0;
}

