package ood.erp.core.UI.form;

import ood.erp.core.resources.InformationResource;

/**
 * Created by garfild on 7/20/16.
 */
public class InformationResourceFormBuilder implements FormBuilder<InformationResource> {
    public Form<InformationResource> construct() {
        Form<InformationResource> form = new Form<InformationResource>();
        ChoiceField<InformationResource.Type> typeChoice = new ChoiceField<InformationResource.Type>();
        typeChoice.choices(InformationResource.Type.values());
        form.add("type", typeChoice);
        form.add("name", Field.FieldType.text);
        form.add("description", Field.FieldType.text);
        return form;
    }
}
