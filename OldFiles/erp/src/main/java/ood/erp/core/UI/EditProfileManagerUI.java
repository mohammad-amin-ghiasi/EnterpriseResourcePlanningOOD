package ood.erp.core.UI;

import ood.erp.core.UI.form.Form;
import ood.erp.core.UI.form.FormBuilder;
import ood.erp.core.users.IUserCatalog;
import ood.erp.core.users.UserData;

/**
 * Created by garfild on 7/20/16.
 */
public class EditProfileManagerUI extends ProfileManagerUI {

    FormBuilder<UserData> formBuilder;
    IUserCatalog userCatalog;
    int userId;

    public EditProfileManagerUI(int uId, IUserCatalog c, FormBuilder<UserData> f) {
        userId = uId;
        userCatalog = c;
        formBuilder = f;
    }

    public void execute() {

        Form<UserData> form = formBuilder.construct();
        form.bind(userCatalog.find(userId).getUserData());
        UserData u = form.ask();
        editProfile(u);
    }

    public void editProfile(UserData userData) {

        userCatalog.find(userId).editProfile(userData);
        userCatalog.commit();
    }
}
