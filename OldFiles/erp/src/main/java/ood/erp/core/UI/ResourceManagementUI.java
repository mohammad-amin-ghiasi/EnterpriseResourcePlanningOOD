package ood.erp.core.UI;

import ood.erp.core.UI.form.Form;
import ood.erp.core.UI.form.FormBuilder;
import ood.erp.core.resources.InformationResource;
import ood.erp.core.resources.ResourceRepository;

/**
 * Created by garfild on 7/20/16.
 */
public class ResourceManagementUI {
    private FormBuilder<InformationResource> formBuilder;
    private ResourceRepository resourceRepository;

    public ResourceManagementUI(FormBuilder<InformationResource> formBuilder, ResourceRepository resourceRepository) {
        this.formBuilder = formBuilder;
        this.resourceRepository = resourceRepository;
    }

    public void execute() {
        Form<InformationResource> form = formBuilder.construct();
        form.bind(new InformationResource());
        InformationResource informationResource = form.ask();
        resourceRepository.addInformationResource(informationResource);

    }
}
