package ood.erp.core.users;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by garfild on 7/20/16.
 */
public class UserCatalog implements IUserCatalog {

    Map<Integer, User> users = new HashMap<Integer, User>();

    private static int counter = 0;

    public User find(int id) {
//        just not to error
        return users.get(id);
    }

    public void add(User user) {
        user.setId(counter);
        users.put(counter, user);
        counter++;

    }

    public void save(User user){

        add(user);
    }

    public void commit() {

    }
}
