package ood.erp.core.UI;

import ood.erp.core.DB.ICatalog;
import ood.erp.core.DB.IIdentified;
import ood.erp.core.UI.form.Form;
import ood.erp.core.UI.form.FormBuilder;

/**
 * Created by garfild on 7/20/16.
 */
public class CreateManagerUI<T extends IIdentified> {

    FormBuilder<T> formBuilder;
    ICatalog<T> catalog;
    T newObject;

    public CreateManagerUI(FormBuilder<T> builder, ICatalog<T> catalog, T newObject){

        formBuilder = builder;
        this.catalog = catalog;
        this.newObject = newObject;
    }

    public T execute(){
        Form<T> form = formBuilder.construct();
        form.bind(newObject);
        T t = form.ask();
        catalog.save(form.ask());
        catalog.commit();
        return t;

    }
}
