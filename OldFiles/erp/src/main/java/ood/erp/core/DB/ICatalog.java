package ood.erp.core.DB;

/**
 * Created by garfild on 7/20/16.
 */
public interface ICatalog<T extends IIdentified> {

    public void commit();
    public void save(T t);
    public T find(int id);
}
