package ood.erp.core.UI.form;

/**
 * Created by garfild on 7/20/16.
 */
public class Form<T> {

    public void bind(T t) {

    }


    /**
     * @param field
     * @param type
     * @return
     */
    public Field add(String field, Field.FieldType type) {
        return null;
    }

    public <M> void add(String field, Field<M> f) {

    }

    public <M> void add(String field, Form<M> form) {

    }

    public <M> void add(String field, FormBuilder<M> form) {

        add(field, form.construct());
    }

    public T ask() {
        return null;
    }

}
