package ood.erp.core.users;

import ood.erp.core.DB.ICatalog;

/**
 * Created by garfild on 7/20/16.
 */
public interface IUserCatalog extends ICatalog<User> {
    public void add(User user);
}
