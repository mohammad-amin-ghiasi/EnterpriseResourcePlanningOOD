package ood.erp.core.UI;

import ood.erp.core.UI.form.Form;
import ood.erp.core.UI.form.FormBuilder;
import ood.erp.core.users.IUserCatalog;
import ood.erp.core.users.User;
import ood.erp.core.users.UserData;

/**
 * Created by garfild on 7/20/16.
 */
public class CreateProfileManagerUI extends ProfileManagerUI {


    FormBuilder<User> formBuilder;
    IUserCatalog userCatalog;

    public CreateProfileManagerUI(IUserCatalog userCatalog, FormBuilder<User> formBuilder) {
//
        this.formBuilder = formBuilder;
        this.userCatalog = userCatalog;
    }

    public void execute() {
//        Form<User> form = formBuilder.construct();
//        form.bind(new User());
//        User u = form.ask();
//        addUser(u);
        Form<User> form = formBuilder.construct();
        User u = new User(new UserData());
        form.bind(u);
        u = form.ask();
        this.addUser(u);
    }

    public void addUser(User u) {
        userCatalog.add(u);
    }
}
