package ood.erp.core.UI.form;

/**
 * Created by garfild on 7/20/16.
 */
public interface FormBuilder<T>{

    public Form<T> construct();

}
