package ood.erp.core.UI.form;

import ood.erp.core.development.Project;

/**
 * Created by garfild on 7/20/16.
 */
public class ProjectFormBuilder implements FormBuilder<Project> {
    public Form<Project> construct() {
        Form<Project> form = new Form<Project>();
        form.add("name", Field.FieldType.text);
        form.add("description", Field.FieldType.text);
        return form;

    }
}
