package ood.erp.core.UI.form;

import ood.erp.core.users.UserData;

/**
 * Created by garfild on 7/20/16.
 */
public class UserDataFormBuilder implements FormBuilder<UserData> {
    
    public Form<UserData> construct(){
        Form<UserData> f = new Form<UserData>();
        f.add("name", Field.FieldType.text).length(20).validate(Field.ValidationType.name);
        f.add("surname", Field.FieldType.text).length(20).validate(Field.ValidationType.name);
        f.add("sshKey", Field.FieldType.text).length(50).validate(Field.ValidationType.ssh);
        return f;
    }
}
