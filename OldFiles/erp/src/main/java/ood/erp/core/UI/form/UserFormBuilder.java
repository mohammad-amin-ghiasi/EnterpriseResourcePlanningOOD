package ood.erp.core.UI.form;

import ood.erp.core.users.User;

/**
 * Created by garfild on 7/20/16.
 */
public class UserFormBuilder implements FormBuilder<User> {

    public Form<User> construct() {
        Form<User> f = new Form<User>();
        f.add("username", Field.FieldType.username);
        f.add("password", Field.FieldType.password);
        f.add("userData", new UserDataFormBuilder());
        return f;
    }
}
