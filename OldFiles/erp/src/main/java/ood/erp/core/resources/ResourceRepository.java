package ood.erp.core.resources;

import ood.erp.core.DB.Catalog;
import ood.erp.core.DB.ICatalog;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by garfild on 7/20/16.
 */
public class ResourceRepository {
    Catalog<InformationResource> informationResourceCatalog;
    Map<Integer, InformationResource> informationResources = new HashMap<Integer, InformationResource>();
    Map<Integer, PhysicalResource> physicalResources = new HashMap<Integer, PhysicalResource>();
    Map<Integer, Asset> assets = new HashMap<Integer, Asset>();
    Map<Integer, Cash> cashes = new HashMap<Integer, Cash>();
    int informationResourceCount = 0;

    public ResourceRepository(Catalog<InformationResource> informationResourceCatalog) {
        this.informationResourceCatalog = informationResourceCatalog;
    }

    public void addInformationResource(InformationResource informationResource) {
        informationResourceCatalog.save(informationResource);
        informationResourceCatalog.commit();
    }
}
