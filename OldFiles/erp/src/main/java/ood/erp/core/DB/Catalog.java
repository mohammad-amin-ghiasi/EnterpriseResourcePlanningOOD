package ood.erp.core.DB;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by garfild on 7/20/16.
 */
public class Catalog<T extends IIdentified> implements ICatalog<T> {

    int counter = 0;
    Map<Integer, T> map = new HashMap<Integer, T>();
    public void commit() {

    }

    public void save(T t){
        t.setId(counter);
        map.put(counter, t);
        counter++;
    }

    public T find(int id){

        return map.get(id);
    }
}
