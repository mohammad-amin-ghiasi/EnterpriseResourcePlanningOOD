<?xml version="1.0" ?><cherrytree><node name="Java ORMS" prog_lang="custom-colors" readonly="False" tags="" unique_id="1"><rich_text>• </rich_text><rich_text link="node 2">JDBC</rich_text><rich_text>
• </rich_text><rich_text link="node 3">Javax.sql</rich_text><rich_text>
• Hibernate
• JOOQ</rich_text><node name="JDBC" prog_lang="custom-colors" readonly="False" tags="" unique_id="2"><rich_text>My  explanation:
It seems that we're gonna use a mannual SQL injection, If we use this kind of library... :) 

</rich_text><rich_text scale="h2">Examples</rich_text><rich_text foreground="#555555" scale="h2">[</rich_text><rich_text link="webs https://en.wikipedia.org/w/index.php?title=Java_Database_Connectivity&amp;action=edit&amp;section=3" scale="h2">edit</rich_text><rich_text foreground="#555555" scale="h2">]</rich_text><rich_text>
When a Java application needs a database connection, one of the DriverManager.getConnection() methods is used to create a JDBC connection. The URL used is dependent upon the particular database and JDBC driver. It will always begin with the &quot;jdbc:&quot; protocol, but the rest is up to the particular vendor.
</rich_text><rich_text justification="left"></rich_text><rich_text>

Starting from Java SE 7 you can use Java's </rich_text><rich_text link="webs http://docs.oracle.com/javase/tutorial/essential/exceptions/tryResourceClose.html">try-with-resources</rich_text><rich_text> statement to make the above code cleaner:
</rich_text><rich_text justification="left"></rich_text><rich_text foreground="#408080">
Once a connection is established, a statement can be created.
</rich_text><rich_text foreground="#408080" justification="left"></rich_text><rich_text foreground="#408080">
</rich_text><rich_text>
Note that Connections, Statements, and ResultSets often tie up </rich_text><rich_text link="webs https://en.wikipedia.org/wiki/Operating_system">operating system</rich_text><rich_text> resources such as sockets or </rich_text><rich_text link="webs https://en.wikipedia.org/wiki/File_descriptor">file descriptors</rich_text><rich_text>. In the case of Connections to remote database servers, further resources are tied up on the server, e.g., </rich_text><rich_text link="webs https://en.wikipedia.org/wiki/Cursor_(databases)">cursors</rich_text><rich_text> for currently open ResultSets. It is vital to close() any JDBC object as soon as it has played its part; </rich_text><rich_text link="webs https://en.wikipedia.org/wiki/Garbage_collection_(computer_science)">garbage collection</rich_text><rich_text> should not be relied upon. The above try-with-resources construct is a code pattern that obviates this.
Data is retrieved from the database using a database query mechanism. The example below shows creating a statement and executing a query.
</rich_text><rich_text justification="left"></rich_text><rich_text>
An example of a PreparedStatement query, using conn and class from first example.
</rich_text><rich_text justification="left"></rich_text><rich_text foreground="#408080">
If a database operation fails, JDBC raises an </rich_text><rich_text link="webs https://docs.oracle.com/javase/8/docs/api/java/sql/SQLException.html">SQLException</rich_text><rich_text>. There is typically very little one can do to recover from such an error, apart from logging it with as much detail as possible. It is recommended that the SQLException be translated into an application domain exception (an unchecked one) that eventually results in a transaction rollback and a notification to the user.
An example of a </rich_text><rich_text link="webs https://en.wikipedia.org/wiki/Database_transaction">database transaction</rich_text><rich_text>:
</rich_text><rich_text justification="left"></rich_text><codebox char_offset="430" frame_height="160" frame_width="700" highlight_brackets="True" show_line_numbers="False" syntax_highlighting="java" width_in_pixels="True">Connection conn = DriverManager.getConnection(
     &quot;jdbc:somejdbcvendor:other data needed by some jdbc vendor&quot;,
     &quot;myLogin&quot;,
     &quot;myPassword&quot; );
try {
     /* you use the connection here */
} finally {
    //It's important to close the connection when you are done with it
    try { conn.close(); } catch (Throwable ignore) { /* Propagate the original exception
instead of this one that you may want just logged */ }
}</codebox><codebox char_offset="537" frame_height="100" frame_width="700" highlight_brackets="True" show_line_numbers="True" syntax_highlighting="java" width_in_pixels="True">try (Connection conn = DriverManager.getConnection(
     &quot;jdbc:somejdbcvendor:other data needed by some jdbc vendor&quot;,
     &quot;myLogin&quot;,
     &quot;myPassword&quot; ) ) {
     /* you use the connection here */
}  // the VM will take care of closing the connection</codebox><codebox char_offset="601" frame_height="100" frame_width="700" highlight_brackets="True" show_line_numbers="True" syntax_highlighting="java" width_in_pixels="True">try (Statement stmt = conn.createStatement()) {
    stmt.executeUpdate( &quot;INSERT INTO MyTable( name ) VALUES ( 'my name' ) &quot; );
}</codebox><codebox char_offset="1211" frame_height="100" frame_width="700" highlight_brackets="True" show_line_numbers="True" syntax_highlighting="java" width_in_pixels="True">try (Statement stmt = conn.createStatement();
    ResultSet rs = stmt.executeQuery( &quot;SELECT * FROM MyTable&quot; )
) {
    while ( rs.next() ) {
        int numColumns = rs.getMetaData().getColumnCount();
        for ( int i = 1 ; i &lt;= numColumns ; i++ ) {
           // Column numbers start at 1.
           // Also there are many methods on the result set to return
           //  the column as a particular type. Refer to the Sun documentation
           //  for the list of valid conversions.
           System.out.println( &quot;COLUMN &quot; + i + &quot; = &quot; + rs.getObject(i) );
        }
    }
}
</codebox><codebox char_offset="1295" frame_height="100" frame_width="700" highlight_brackets="True" show_line_numbers="True" syntax_highlighting="java" width_in_pixels="True">try (PreparedStatement ps =
    conn.prepareStatement( &quot;SELECT i.*, j.* FROM Omega i, Zappa j WHERE i.name = ? AND j.num = ?&quot; )
){
    // In the SQL statement being prepared, each question mark is a placeholder
    // that must be replaced with a value you provide through a &quot;set&quot; method invocation.
    // The following two method calls replace the two placeholders; the first is
    // replaced by a string value, and the second by an integer value.
    ps.setString(1, &quot;Poor Yorick&quot;);
    ps.setInt(2, 8008);

    // The ResultSet, rs, conveys the result of executing the SQL statement.
    // Each time you call rs.next(), an internal row pointer, or cursor,
    // is advanced to the next row of the result.  The cursor initially is
    // positioned before the first row.
    try (ResultSet rs = ps.executeQuery()) {
        while ( rs.next() ) {
            int numColumns = rs.getMetaData().getColumnCount();
            for ( int i = 1 ; i &lt;= numColumns ; i++ ) {
                // Column numbers start at 1.
                // Also there are many methods on the result set to return
                // the column as a particular type. Refer to the Sun documentation
                // for the list of valid conversions.
                System.out.println( &quot;COLUMN &quot; + i + &quot; = &quot; + rs.getObject(i) );
            } // for
        } // while
    } // try
} // try</codebox><codebox char_offset="1715" frame_height="115" frame_width="700" highlight_brackets="True" show_line_numbers="True" syntax_highlighting="java" width_in_pixels="True">boolean autoCommitDefault = conn.getAutoCommit();
try {
    conn.setAutoCommit(false);

    /* You execute statements against conn here transactionally */

    conn.commit();
} catch (Throwable e) {
    try { conn.rollback(); } catch (Throwable ignore) {}
    throw e;
} finally {
    try { conn.setAutoCommit(autoCommitDefault); } catch (Throwable ignore) {}
}</codebox></node><node name="Javax" prog_lang="custom-colors" readonly="False" tags="" unique_id="3"><rich_text>Javasx :
It seems it's used for server side databases :) </rich_text></node><node name="JOOQ" prog_lang="custom-colors" readonly="False" tags="" unique_id="4"><rich_text>JOOQ:
It has an available open source version :) 


It makes the followin code : 
</rich_text><rich_text justification="left"></rich_text><rich_text>
from this
</rich_text><rich_text justification="left"></rich_text><rich_text>

or for example

turns into:
</rich_text><codebox char_offset="82" frame_height="130" frame_width="700" highlight_brackets="True" show_line_numbers="True" syntax_highlighting="java" width_in_pixels="True">Result&lt;Record&gt; result =
create.select()
      .from(AUTHOR.as(&quot;a&quot;))
      .join(BOOK.as(&quot;b&quot;)).on(a.ID.equal(b.AUTHOR_ID))
      .where(a.YEAR_OF_BIRTH.greaterThan(1920)
      .and(a.FIRST_NAME.equal(&quot;Paulo&quot;)))
      .orderBy(b.TITLE)
      .fetch();</codebox><codebox char_offset="94" frame_height="130" frame_width="700" highlight_brackets="True" show_line_numbers="True" syntax_highlighting="java" width_in_pixels="True">-- Select all books by authors born after 1920,
-- named &quot;Paulo&quot; from a catalogue:
SELECT *
  FROM author a
  JOIN book b ON a.id = b.author_id
 WHERE a.year_of_birth &gt; 1920
   AND a.first_name = 'Paulo'
 ORDER BY b.title</codebox><codebox char_offset="112" frame_height="100" frame_width="700" highlight_brackets="True" show_line_numbers="True" syntax_highlighting="java" width_in_pixels="True">-- get all authors' first and last names, and the number
-- of books they've written in German, if they have written
-- more than five books in German in the last three years
-- (from 2011), and sort those authors by last names
-- limiting results to the second and third row, locking
-- the rows for a subsequent update... whew!

  SELECT AUTHOR.FIRST_NAME, AUTHOR.LAST_NAME, COUNT(*)
    FROM AUTHOR
    JOIN BOOK ON AUTHOR.ID = BOOK.AUTHOR_ID
   WHERE BOOK.LANGUAGE = 'DE'
     AND BOOK.PUBLISHED &gt; '2008-01-01'
GROUP BY AUTHOR.FIRST_NAME, AUTHOR.LAST_NAME
  HAVING COUNT(*) &gt; 5
ORDER BY AUTHOR.LAST_NAME ASC NULLS FIRST
   LIMIT 2
  OFFSET 1
     FOR UPDATE</codebox><codebox char_offset="126" frame_height="100" frame_width="700" highlight_brackets="True" show_line_numbers="True" syntax_highlighting="java" width_in_pixels="True">// And with jOOQ...



DSLContext create = DSL.using(connection, dialect);

create.select(AUTHOR.FIRST_NAME, AUTHOR.LAST_NAME, count())
      .from(AUTHOR)
      .join(BOOK).on(BOOK.AUTHOR_ID.equal(AUTHOR.ID))
      .where(BOOK.LANGUAGE.equal(&quot;DE&quot;))
      .and(BOOK.PUBLISHED.greaterThan(&quot;2008-01-01&quot;))
      .groupBy(AUTHOR.FIRST_NAME, AUTHOR.LAST_NAME)
      .having(count().greaterThan(5))
      .orderBy(AUTHOR.LAST_NAME.asc().nullsFirst())
      .limit(2)
      .offset(1)
      .forUpdate()
      .fetch();</codebox></node></node></cherrytree>