package ood.erp.core.resources;

import java.util.Date;

import ood.erp.core.Interval;
import ood.erp.core.development.Development;

public class Allocation {

	//protected String resourceType; is derived from self::allocated
	protected Development allocatedTo;
	protected Resource allocated;
	protected Interval interval;
	protected boolean necesarry;
	
	public boolean canBeShared(){
		
		return allocated.shareable();
	}
	
	public boolean isOpenEnded(){
		
		return interval.isOpenEnded();
	}
	
	public boolean finish(){
		// TODO fill this in, signifies the end of this allocation
	}
}
