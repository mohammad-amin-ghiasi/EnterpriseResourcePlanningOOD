package ood.erp.core.resources;

import ood.erp.core.development.Module;

/**
 * since java doesn't support multiple inheritance, we need this, to integrate
 * modules into the Resource Management system.
 * 
 * @author farzad
 *
 */
public class ModuleResource extends InformationResource {

	protected Module module;

}
