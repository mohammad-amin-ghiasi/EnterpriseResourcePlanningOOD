package ood.erp.core;

import java.util.Date;

public class Interval {

	protected Date startDate;
	protected Date endDate;
	
	public Interval(Date startD, Date endD){
		
		startDate = startD;
		endDate = endD;
	}
	
	protected Interval(Date startD){
		
		startDate = startD;
		endDate = null;
	}
	
	public static Interval openEnded(Date startDate){
		
		return new Interval(startDate);
	}
	
	public boolean isOpenEnded(){
		
		return endDate == null;
	}
	
	
}
