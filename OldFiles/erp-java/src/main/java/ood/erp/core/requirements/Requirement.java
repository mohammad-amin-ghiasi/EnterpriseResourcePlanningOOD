package ood.erp.core.requirements;

public abstract class Requirement {

	/**
	 * whether this requirement has been met (provided).
	 */
	protected boolean met = false;
}
