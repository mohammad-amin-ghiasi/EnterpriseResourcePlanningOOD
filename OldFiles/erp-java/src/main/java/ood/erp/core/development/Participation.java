package ood.erp.core.development;

import ood.erp.core.collaborators.CollaboratingParty;
import ood.erp.core.collaborators.Job;

public class Participation {

	protected CollaboratingParty collaborator;
	/**
	 * the job, the collaborator is responsible for in the Development of the
	 * artifact(this.developed).
	 */
	protected Job as;

	protected Developed developed;
}
