package ood.erp.core.development;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.hibernate.annotations.Parent;

public class Project extends Developed {

	public enum Size {

		small(10), medium(40), big(200);
		public final int value;

		private Size(int v) {
			value = v;
		}
	}

	protected String description;
	protected ArrayList<Estimation> estimations;

	protected Set<System> systems;

	public Project(String n, String desc) {
		name = n;
		description = desc;
		estimations = new ArrayList<Estimation>();
		systems = new HashSet<System>();
	}

	public void addEstimation(Estimation e) {
		estimations.add(e);
	}

	public void removeEstimation(Estimation e) {
		estimations.remove(e);
	}

	public void editDescription(String d) {
		description = d;
	}

	public void editName(String n) {
		name = n;
	}

	public void addSystem(System s) {

		systems.add(s);
	}

	public void showDetails() {

		// TODO @KAZEM implement this
	}

	@Override
	protected Iterable<? extends Developed> getDevelopedChildren() {

		return systems;
	}


	public Size getSize() {
		// TODO implement this based on the total size of constituting systems
	}
}

