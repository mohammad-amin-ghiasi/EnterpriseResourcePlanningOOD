package ood.erp.core.resources;

import java.util.ArrayList;
import java.util.Set;

public abstract class Resource {

	protected Set<Allocation> allocations;
	
	public boolean shareable(){
		
		return false;
	}
	
	public boolean reclaimable(){
		
		return true;
	}
}
