package ood.erp.core.users;

public abstract class Position {

	protected String title;
	
	/*
	 * this is supposed to replace `getPrivileges()` in the class Diagram, must
	 * be determined based on the underlying role hierarchy
	 */
	abstract protected boolean hasRole(String role);
}
