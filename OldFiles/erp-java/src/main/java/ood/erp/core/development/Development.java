package ood.erp.core.development;

import java.util.Set;

import ood.erp.core.Interval;
import ood.erp.core.collaborators.CollaboratingParty;
import ood.erp.core.resources.Allocation;

public abstract class Development {

	protected Interval activitySpan;// same as 'interval' in the class diagram
	protected DevelopmentActivity activity;
	protected Developed target;
	protected Set<Allocation> allocatedResources;
	protected Set<Participation> participations;
	protected short priority;
	protected boolean finished;

	public String getActivityName() {

		return activity.name();
	}

	public Interval getActivitySpan() {

		return activitySpan;
	}

	public void finish() {

		finished = true;
	}

	public boolean finished() {

		return finished;
	}

	public boolean isParticipating(CollaboratingParty p) {
		// TODO implement this based on this.participations
	}

}
