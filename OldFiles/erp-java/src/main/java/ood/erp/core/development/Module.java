package ood.erp.core.development;

public class Module extends Developed {

	public enum Size {

		small(1), medium(2), big(3);
		/**
		 * this is used to estimate project and system sizes
		 */
		public final int value;

		private Size(int v) {
			value = v;
		}

	}

	protected Size size;

	protected Usage usage;

	@Override
	protected Iterable<? extends Developed> getDevelopedChildren() {

		return null;
	}

}
