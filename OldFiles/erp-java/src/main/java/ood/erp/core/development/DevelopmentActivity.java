package ood.erp.core.development;

public enum DevelopmentActivity {

	requirements,
	analysis,
	design,
	construction,
	maintenance
}
