package ood.erp.core.development;

import java.util.Iterator;
import java.util.Set;

public class System extends Developed {

	public enum Size {
		small(5), medium(10), big(15);

		public final int value;

		private Size(int v) {
			value = v;
		}
	}

	protected Set<String> technology;
	protected Set<Module> modules;

	public void addModule(Module m) {

		if (modules.contains(m))
			throw new RuntimeException("the system " + this
					+ "already contains the module " + m);
	}

	@Override
	protected Iterable<? extends Developed> getDevelopedChildren() {

		return modules;
	}

	public void searchTextSize() {

		// TODO @KAZEM, handle this shit
	}

	@Override
	public String toString() {

		return name;
	}

	public Size getSize() {
		// TODO implement this based on the size of the constituting modules
	}
}
