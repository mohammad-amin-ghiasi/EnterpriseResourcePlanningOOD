package ood.erp.core.development;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Set;

import ood.erp.core.collaborators.CollaboratingParty;
import ood.erp.core.requirements.Requirement;

public abstract class Developed {

	/**
	 * whatever this is, it's gotta have a name
	 */
	protected String name;

	/**
	 * whether the construction of the artifact is finished
	 */
	protected boolean constructed;
	
	/**
	 * the requirements of this artifact, we use an array list since order is
	 * (could be) relevant here
	 */
	protected ArrayList<Requirement> requirements;
	
	/**
	 * the different development phases of this artifact. we use an array list
	 * the same reason we're using one for requirements.
	 */
	protected ArrayList<Development> developments;
	

	/**
	 * @return whether there are any unfinished developments for this artifact
	 */
	public boolean isBeingDeveloped(){
		
		for (Iterator iterator = developments.iterator(); iterator.hasNext();) {
			Development development = (Development) iterator.next();
			if (!development.finished()) {
				return true;
			}
		}

		return false;
	}
	
	public void addRequirement(Requirement r){
		
		requirements.add(r);
	}
	
	public void removeRequirement(Requirement r){
		
		// TODO might have to do this a little differently, e.g: for a cash requirement `r`, find a similar requirement in requirements, and remove it, or maybe I can keep the agnosticism here and let other people handle such things
		requirements.remove(r);
	}
	
	public boolean isParticipating(CollaboratingParty p) {

		for (Iterator iterator = developments.iterator(); iterator.hasNext();) {
			Development development = (Development) iterator.next();
			if (development.isParticipating(p)) {
				return true;
			}
		}

		Iterable<? extends Developed> children = getDevelopedChildren();
		if (children != null) {
			for (Iterator iterator = children.iterator(); iterator.hasNext();) {
				Developed child = (Developed) iterator.next();
				if (child.isParticipating(p)) {
					return true;
				}
			}
		}

		return false;
	}

	public boolean hasAccess(CollaboratingParty p) {

		// TODO implement this, probably something like isParticipating based on
		// `hasRole` of CollaboratingParty
	}

	protected abstract Iterable<? extends Developed> getDevelopedChildren();

}
