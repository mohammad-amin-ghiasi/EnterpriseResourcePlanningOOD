from tkinter import *
from tkinter.ttk import *

from components import *


def t(something):
    return something


def add_hydrate_button(frame, obj):
    return Button(frame, text="hydrate", command=lambda: print(obj.hydrate))


class BaseFrame(Frame):
    """ this is an abstract class, that contains helpers for frames"""

    def combo(self, values, textvariable=None, height=10, frame=None, pack=False, side="top"):
        """
        values: tuple of values
        textvariable: the StringVar to bind the data to
        frame: Frame the frame that it should be put in
        pack: whether to pack it at `side`
        return: the combobox instance
        """
        if frame is None:
            frame = self
        if textvariable is None:
            textvariable = StringVar()

        c = Combobox(frame, height=height, values=values, textvariable=textvariable)
        if pack:
            c.pack(side=side)
        return c

    def add_accept(self, callback=None, frame=None):
        if frame is None:
            frame = self
        if callback is None:
            callback = self.accept
        self.accept_button = Button(frame, text=t("accept"), command=callback)


class RequirementManagerUI(BaseFrame):
    requirement_types = ('cash', 'physical resource', 'human resource', 'information resource')

    def get_requirement_types(self):
        return RequirementManagerUI.requirement_types

    def __init__(self, master, module_catalog):
        super().__init__(master)
        self.requirement_frames = []
        self.master = master
        self.module_catalog = module_catalog
        self.pack()
        self.create_widgets()
        self.add_accept()
        self.accept_button.pack(side="bottom")

    def create_widgets(self):
        Label(self, text="Choose Module:").pack(side="top")
        self.module_combo = self.combo(self.get_module_choices(), pack=True, side="left")
        self.requirement_forms = []
        self.add_add_requirement_widget()

    def get_chosen_module(self):
        return self.modules[self.module_combo.current()]

    def get_module_choices(self):
        self.modules = self.module_catalog.find_all()
        return ["{{ {}, {}: {} }}".format(m.get_id(), m.get_name(), m.get_description()) for m in
                self.modules]

    def add_name_widget(self):
        label = Label(self, text="name")
        label.pack()

    def add_add_requirement_widget(self):
        """we only create the button that creates additional requirement widgets"""
        self.add_requirement_button = Button(self, text=t('new requirement'),
                                             command=self.add_new_requirement_widget)
        self.add_requirement_button.pack()

    def add_new_requirement_widget(self):
        frame = Frame(self)
        requirement_choices = [t(x) for x in self.get_requirement_types()]
        var = StringVar()
        requirement_type_choice = Combobox(frame, height=10, values=requirement_choices,
                                           textvariable=var)
        new_item = {'type_choice': requirement_type_choice, 'type_var': var}
        self.requirement_forms.append(new_item)
        requirement_type_choice.bind('<<ComboboxSelected>>',
                                     lambda event: self.add_specific_requirement_frame(frame,
                                                                                       self.get_requirement_types()[
                                                                                           requirement_type_choice.current()]))
        requirement_type_choice.pack(side="left")
        frame.pack()

    def add_specific_requirement_frame(self, frame, requirement_type):
        """ must return a *RequirementFrame instance"""
        print(requirement_type)
        f = None
        if requirement_type == 'human resource':
            f = HumanResourceRequirementFrame(frame)
            f.pack(side="left")
        elif requirement_type == 'cash':
            f = CashRequirementFrame(frame)
            f.pack(side="left")
        elif requirement_type == 'physical resource':
            f = PhysicalResourceRequirementFrame(frame)
            f.pack(side="left")
        elif requirement_type == 'information resource':
            print("information resource")
            f = InformationResourceRequirementFrame(frame, self.module_catalog)
            f.pack(side="left")
        if f is not None:
            self.requirement_frames.append(f)

    def accept(self):
        module = self.get_chosen_module()
        for f in self.requirement_frames:
            module.addRequirement(f.hydrate())


class ResourceRequirementFrame(BaseFrame):
    """ This is an Abstract class and shouldn't be instantiated directly
    child classes should implement add_widgets() """

    def __init__(self, master):
        super().__init__(master)
        self.add_widgets()

    def add_widgets(self): pass

    def type_combo(self, types, textvariable=None, frame=None, pack=True, side="left"):
        c = self.combo(types, textvariable=textvariable, height=10, frame=frame, pack=pack,
                       side=side)
        return c


class TypedResourceRequirementFrame(ResourceRequirementFrame):
    """ the Corresponding requirement class MUST
    implement `set_type(type)` and `set_count(count)`
    """

    def get_types(self): pass

    def get_requirement_instance(self): pass

    def add_widgets(self):
        var = StringVar()
        c = Combobox(self, height=10, values=self.get_types(), textvariable=var)
        self.type_widget = c
        self.count = StringVar()
        self.count_widget = Entry(self, textvariable=self.count)
        Label(self, text=t('type')).pack(side="left")
        self.type_widget.pack(side="left")
        Label(self, text=t('count')).pack(side="left")
        self.count_widget.pack(side="left")
        add_hydrate_button(self, self).pack(side="left")

    # ----- [BEGIN] hydration
    def get_count(self):
        return int(self.count.get())

    def get_type(self):
        return self.get_types()[self.type_widget.current()]

    def hydrate(self):
        h = self.get_requirement_instance()
        h.set_type(self.get_type())
        h.set_count(self.get_count())
        return h


# ----- [END] hydration

class HumanResourceRequirementFrame(TypedResourceRequirementFrame):
    def get_types(self):
        return HumanResource.TYPES

    def get_requirement_instance(self):
        return HumanResourceRequirement(None, None)


class PhysicalResourceRequirementFrame(TypedResourceRequirementFrame):
    def get_types(self):
        return PhysicalResource.TYPES

    def get_requirement_instance(self):
        return PhysicalResourceRequirement(None, None)


class InformationResourceRequirementFrame(ResourceRequirementFrame):
    """ needs a type, currently only module supported then a widget must be displayed to choose the mosule"""

    def __init__(self, master, module_catalog):
        super().__init__(master)
        self.module_catalog = module_catalog

    def add_widgets(self):
        self.specific_information_resource_type_frame = None
        c = self.type_combo(InformationResource.TYPES)
        print(InformationResource.TYPES)
        self.type_choice_widget = c
        c.bind('<<ComboboxSelected>>',
               lambda event: self.display_specific_information_resource_type_widgets())
        add_hydrate_button(self, self).pack(side="left")

    def display_specific_information_resource_type_widgets(self):
        if self.specific_information_resource_type_frame is not None:
            self.specific_information_resource_type_frame.pack_forget()
        self.specific_information_resource_type_frame = Frame(self)
        self.specific_information_resource_type_frame.pack(side="left")

        t = self.get_type()

        if t == InformationResource.TYPE_MODULE:
            self.add_module_widgets()

    def add_module_widgets(self):

        self.module_combo = self.combo(self.get_module_choices(),
                                       frame=self.specific_information_resource_type_frame,
                                       pack=True, side="left")

    def get_chosen_module(self):
        return self.modules[self.module_combo.current()]

    def get_module_choices(self):
        self.modules = self.module_catalog.find_all()
        return ["{{{}, {}: {}}}".format(m.get_id(), m.get_name(), m.get_description()) for m in
                self.modules]

    def get_type(self):
        return InformationResource.TYPES[self.type_choice_widget.current()]

    def get_id(self):
        if self.get_type == InformationResource.TYPE_MODULE:
            return self.get_chosen_module.get_id()

    def hydrate(self):
        return InformationResourceRequirement(type=self.get_type(), resource_id=self.get_id())


class CashRequirementFrame(ResourceRequirementFrame):
    """ we probably only need the amount"""

    def add_widgets(self):
        self.currency_choice = self.combo(self.get_currency_choices(), pack=True, side="left")
        self.quantity_var = StringVar()
        Entry(self, textvariable=self.quantity_var).pack(side="left")
        add_hydrate_button(self, self).pack(side="left")

    def get_currency_choices(self):
        return ('dollars', 'euros')
        # return Cash.CURRENCIES

    def get_quantity(self):
        return float(self.quantity_var.get())

    def get_currency(self):
        return self.get_currency_choices()[self.currency_choice.current()]

    def hydrate(self):
        return CashRequirement(quantity=self.get_quantity(), currency=self.get_currency())


class AssetRequirementFrame(ResourceRequirementFrame): pass


""" this type of requirement will not be supported, since it's a financial resource and no project
can require this, they can only require cash"""

if __name__ == '__main__':
    root = Tk()
    app = RequirementManagerUI(root, ModuleCatalog.getInstance())
    root.mainloop()
