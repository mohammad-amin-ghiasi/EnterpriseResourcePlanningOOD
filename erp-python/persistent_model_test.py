from sqlalchemy import Column, Integer, String

from database import Database


class TestClass(Database.get_instance().get_base()):
    _name_length = 250
    __tablename__ = 'testTable'
    id = Column(Integer, primary_key=True)
    name = Column(String(_name_length), nullable=False)


Database.get_instance().add_transaction(TestClass(name='Mahshad'))
Database.get_instance().add_transaction(TestClass(name='Hamid'))

a = TestClass(name='ququli')
Database.get_instance().add_transaction(a)

a.name = 'qaqul'

Database.get_instance().get_session().commit()
