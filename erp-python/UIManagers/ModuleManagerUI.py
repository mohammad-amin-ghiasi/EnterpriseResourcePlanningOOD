import tkinter as tk


class ModuleManagerUI:
    def __init__(self):
        pass

    def execute(self, data):
        pass


class AddModuleFrame(tk.Frame):
    def __init__(self, master=None):
        super().__init__(master)
        self.pack()
        self.create_widgets()
        self.module_manager = ModuleManagerUI()

    def create_widgets(self):
        self.name_input = tk.Entry(self)
        self.name_input.pack()
        self.usage_input = tk.Entry(self)
        self.usage_input.pack()
        self.submit_button = tk.Button(self, text="submit")
        self.submit_button['command'] = self.submit
        self.submit_button.pack()
        self.quit = tk.Button(self, text="QUIT", fg="red", command=root.destroy)
        self.quit.pack(side="bottom")

    def submit(self):
        data = {}
        data['name'] = self.name_input.get()
        data['usage'] = self.usage_input.get()
        self.module_manager.execute(data)


root = tk.Tk()
app = AddModuleFrame(master=root)
app.mainloop()
