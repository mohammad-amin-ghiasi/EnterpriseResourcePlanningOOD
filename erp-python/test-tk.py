import tkinter as tk
import tkinter.ttk as ttk

class TestDynamicFields(tk.Frame):
    def __init__(self, master):
        super().__init__(master)
        self.pack()
        self.create_widgets()
        #self.add_field()

    def create_widgets(self):
        self.add_button = tk.Button(self, text='add', command=self.add_field)
        self.add_button.pack(side="top")
        self.dynamic_fields = []
        self.print_button = tk.Button(self, text='print', command=self.print)
        self.print_button.pack(side="bottom")

    def add_field(self):
        values = ('one', 'two')
        var = tk.StringVar()
        t = ttk.Combobox(self, height=10, values=values, textvariable=var)
        self.dynamic_fields.append(var)
        t.pack(side="top")
        print(var.get())

    def print(self):
        for v in self.dynamic_fields:
            print(v.get())

root = tk.Tk()
app = TestDynamicFields(root)
root.mainloop()
