class InformationResource:
    TYPE_MODULE = 'module'
    TYPES = (TYPE_MODULE,)


class PhysicalResource:
    TYPES = []  # this must be initialized at the bootstrap

    def __init__(self, type=None, id=None):
        self.type = None  # see next line
        self.set_type(type)
        self.id = id

    def set_type(self, type):
        self.type = type
        if not type in PhysicalResource.TYPES:
            PhysicalResource.TYPES.apppend(type)


class Asset:
    TYPE_PROPERTY = 'property'  # land
    TYPES = (TYPE_PROPERTY)


class HumanResource:
    TYPE_PROGRAMMER = 'programmer'
    TYPE_ANALYST = 'analyst'
    TYPE_DESIGNER = 'designer'
    TYPE_PROJECT_MANAGER = 'project manager'

    TYPES = (TYPE_PROJECT_MANAGER, TYPE_PROGRAMMER, TYPE_ANALYST, TYPE_DESIGNER)

    def __init__(self, type=None):
        self.type = type


class Cash:
    def __init__(self, quantity, currency):
        self.quantity = quantity
        self.currency = currency
