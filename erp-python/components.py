# TODO: We should make it abstract.
from sqlalchemy import Column, Integer, String, Boolean, ForeignKey
from sqlalchemy.orm import relationship
from sqlalchemy_utils.types.choice import ChoiceType

from database import Database

Base = Database.get_instance().get_base()

_name_length = 250
_description_length = _name_length


class BaseCatalog:
    _instance = None
    _catalog_class = None

    def find_all(self):
        session = Database.get_instance().get_session()
        objects = session.query(self._catalog_class).all()
        return objects


class Developed(Base):
    __tablename__ = 'developedTable'
    id = Column(Integer, primary_key=True)
    name = Column(String(_name_length), nullable=False)
    constructed = Column(Boolean, default=False)
    type = Column(String(50))
    requirements = relationship("Requirement")

    __mapper_args__ = {
        'polymorphic_identity': 'developed',
        'polymorphic_on': type
    }

    def isBeingDeveloped(self):
        pass

    def isParticipating(self):
        pass

    def get_id(self):
        return self.id

    def get_name(self):
        return self.name

    def get_constructed(self):
        self.constructed


class Project(Developed, Base):
    __tablename__ = 'projectTable'
    __mapper_args__ = {
        'polymorphic_identity': 'project'
    }
    id = Column(Integer, ForeignKey('developedTable.id'), primary_key=True)


class Module(Developed, Base):
    __tablename__ = 'moduleTable'
    description = Column(String(_description_length), nullable=True)
    __mapper_args__ = {
        'polymorphic_identity': 'module'
    }
    id = Column(Integer, ForeignKey('developedTable.id'), primary_key=True)
    system_id = Column(Integer, ForeignKey('systemTable.id'))
    system = relationship("System", foreign_keys=[system_id])
    usage = Column(String(_name_length), nullable=False)

    def addRequirement(self, requirement):
        session = Database.get_instance().get_session()
        print('======================================')
        print(requirement)
        print('======================================')
        self.requirements.append(requirement)
        session.commit()
        return requirement

    def get_description(self):
        return self.description

    def __str__(self):
        session = Database.get_instance().get_session()
        session.add(self)
        ret = '{system}::{module} with usage {usage}'.format(system=self.system, module=self.name,
                                                             usage=self.usage)
        return ret


class ModuleCatalog(BaseCatalog):
    _catalog_class = Module

    @staticmethod
    def getInstance():
        """
        :rtype: ModuleCatalog
        """
        if ModuleCatalog._instance is None:
            ModuleCatalog._instance = ModuleCatalog()
        return ModuleCatalog._instance

    def addModule(self, module):
        session = Database.get_instance().get_session()
        session.add(module)
        session.commit()
        return module

    def save(self, module):
        session = Database.get_instance().get_session()
        session.add(module)
        session.commit()


class System(Developed, Base):
    __tablename__ = 'systemTable'
    __mapper_args__ = {
        'polymorphic_identity': 'system'
    }
    id = Column(Integer, ForeignKey('developedTable.id'), primary_key=True)

    def __str__(self):
        return '{name}'.format(name=self.name)

    # TODO: Add user database later !
    def addModule(self, loggedInUser):
        pass


class SystemCatalog(BaseCatalog):
    _catalog_class = System

    @staticmethod
    def getInstance():
        """
        @Farzad
        :rtype: SystemCatalog
        """
        if SystemCatalog._instance is None:
            SystemCatalog._instance = SystemCatalog()
        return SystemCatalog._instance

    def getSystemById(self, systemId):
        session = Database.get_instance().get_session()
        system = session.query(System).get(systemId)
        return system


class Requirement(Base):
    __tablename__ = 'requirementTable'
    id = Column(Integer, primary_key=True)
    requirement_type = Column(String(50))
    parent_id = Column(Integer, ForeignKey('developedTable.id'))
    __mapper_args__ = {
        'polymorphic_identity': 'requirement',
        'polymorphic_on': requirement_type
    }

    def __str__(self):
        return '{type}'.format(type=self.type)


class Cash(Base):
    DOLLAR_TYPE = 'Dollars'
    EURO_TYPE = 'Euro'
    RIAL_TYPE = 'Rials'
    CURRENCIES = [DOLLAR_TYPE, EURO_TYPE, RIAL_TYPE]

    def __init__(self, quantity, currency):
        self.quantity = quantity
        self.currency = currency

    __tablename__ = 'cashTable'
    id = Column(Integer, primary_key=True)
    name = Column(String(_name_length), nullable=False)
    currency = Column(ChoiceType([(x, x) for x in CURRENCIES]))
    quantity = Column(Integer)


class CashRequirement(Requirement):
    """ @amin also watch the base class for all requirements [ok]
    note that this has changed from the class diagram, the quantity field 
    is split into it's parts, namely 'quantity' and 'currency', [ok]
    the same should be done for the 'Cash' Class. [ok]
    also there is no association from CashRequirement to Cash
    as in the Class Diagram [ok]"""

    def __init__(self, quantity, currency):
        self.quantity = quantity
        self.currency = currency

    __tablename__ = 'cashRequirementTable'
    __mapper_args__ = {
        'polymorphic_identity': 'cash_requirement'
    }
    id = Column(Integer, ForeignKey('requirementTable.id'), primary_key=True)
    quantity = Column(Integer)
    currency = Column(ChoiceType([(x, x) for x in Cash.CURRENCIES]))

    def __str__(self):
        return 'Cash requirement with value: {quantity}{currency}'.format(quantity=self.quantity,
                                                                          currency=self.currency)


# class AssetRequirement(Requirement):
#     """ @amin this class must be removed, [ok]
#     since Developed can only have a Cash
#     requirement, and does not make sense for
#     them to have Asset requirements"""
#     __tablename__ = 'assetRequirementTable'
#     __mapper_args__ = {
#         'polymorphic_identity': 'asset_requirement'
#     }
#     id = Column(Integer, ForeignKey('requirementTable.id'), primary_key=True)


class HumanResourceRequirement(Requirement):
    """ @amin 'type' is the same as 'job' in the Class Diagram
    too hard to change the name now [ok]"""

    def __init__(self, type, count):
        self.type = type
        self.count = count

    def set_type(self, type):
        self.type = type

    def set_count(self, count):
        self.count = count

    def __str__(self):
        return "{count} Poeple as {type}".format(type=self.type, count=self.count)

    __tablename__ = 'humanResourceRequirementTable'
    __mapper_args__ = {
        'polymorphic_identity': 'human_requirement'
    }
    id = Column(Integer, ForeignKey('requirementTable.id'), primary_key=True)
    type = Column(String(_name_length), nullable=False)


class PhysicalResourceRequirement(Requirement):
    """ @amin """

    def __init__(self, type, count):
        self.type = type
        self.count = count

    def set_type(self, type):
        self.type = type

    def set_count(self, count):
        self.count = count

    __tablename__ = 'physicalResourceRequirementTable'
    __mapper_args__ = {
        'polymorphic_identity': 'physical_requirement'
    }
    id = Column(Integer, ForeignKey('requirementTable.id'), primary_key=True)
    type = Column(String(_name_length), nullable=False)
    count = Column(Integer)

    def __str__(self):
        return '{count} Resources of type {type}'.format(count=self.count, type=self.type)


class InformationResourceRequirement(Requirement):
    """ @amin """

    def __init__(self, type, resource_id):
        self.type = type
        self.resource_id = resource_id
        """ same as resource_id of 'InformationResource' """

    __tablename__ = 'informationResourceRequirementTable'
    __mapper_args__ = {
        'polymorphic_identity': 'information_requirement'
    }
    id = Column(Integer, ForeignKey('requirementTable.id'), primary_key=True)
    type = Column(String(_name_length))
    resource_id = Column(Integer)

    def __str__(self):
        return '{type} Information resource with id {id}'.format(type=self.type,
                                                                 id=self.resource_id)


class RequirementCatalog(BaseCatalog):
    _catalog_class = Requirement

    @staticmethod
    def getInstance():
        """
        :rtype: RequirementCatalog
        """
        if RequirementCatalog._instance is None:
            RequirementCatalog._instance = RequirementCatalog()
        return RequirementCatalog._instance

    def addRequirement(self, requirement):
        session = Database.get_instance().get_session()
        session.add(requirement)
        session.commit()
        return requirement


class User(Base):
    __tablename__ = 'userTable'
    id = Column(Integer, primary_key=True)
    name = Column(String(_name_length), nullable=False)


class UserDate(Base):
    __tablename__ = 'userDateTable'
    id = Column(Integer, primary_key=True)
    name = Column(String(_name_length), nullable=False)


class ExecutiveManager(Base):
    __tablename__ = 'executiveManagerTable'
    id = Column(Integer, primary_key=True)
    name = Column(String(_name_length), nullable=False)


class Position(Base):
    __tablename__ = 'positionTable'
    id = Column(Integer, primary_key=True)
    name = Column(String(_name_length), nullable=False)


class RegularEmployee(Base):
    __tablename__ = 'regularEmployeeTable'
    id = Column(Integer, primary_key=True)
    name = Column(String(_name_length), nullable=False)


class MiddleManager(Base):
    __tablename__ = 'middleManagerTable'
    id = Column(Integer, primary_key=True)
    name = Column(String(_name_length), nullable=False)


class ProjectAssignment(Base):
    __tablename__ = 'projectAssignmentTable'
    id = Column(Integer, primary_key=True)
    name = Column(String(_name_length), nullable=False)


class Job(Base):
    __tablename__ = 'jobTable'
    id = Column(Integer, primary_key=True)
    name = Column(String(_name_length), nullable=False)


class ThirdPratyCollaborator(Base):
    __tablename__ = 'thirdPratyCollaboratorTable'
    id = Column(Integer, primary_key=True)
    name = Column(String(_name_length), nullable=False)


class Group(Base):
    __tablename__ = 'groupTable'
    id = Column(Integer, primary_key=True)
    name = Column(String(_name_length), nullable=False)


class Developement(Base):
    __tablename__ = 'developementTable'
    id = Column(Integer, primary_key=True)
    name = Column(String(_name_length), nullable=False)


class CollaboratingParties(Base):
    __tablename__ = 'collaboratingPartiesTable'
    id = Column(Integer, primary_key=True)
    name = Column(String(_name_length), nullable=False)


class InformationResource(Base):
    TYPE_MODULE = 'module'
    TYPES = (TYPE_MODULE,)

    def __init__(self, resource_id):
        self.resource_id = resource_id
        """ resource_id is the actual id of the linked resource, whose type depends on self.type, e.g: the id of the module if self.type==TYPE_MODULE"""

    __tablename__ = 'informationResourceTable'
    id = Column(Integer, primary_key=True)
    name = Column(String(_name_length), nullable=False)


class PhysicalResource(Base):
    """ @amin """
    TYPES = [ 'Laptop', 'Printer',
              'PowerServer']  # this must be initialized at the bootstrap

    def __init__(self, type=None, resource_id=None):
        self.type = None  # see next line
        self.set_type(type)
        self.resource_id = resource_id
        """ we also need to have a resource_id field, that is the real world id of the thing"""

    __tablename__ = 'physicalResourceTable'
    id = Column(Integer, primary_key=True)
    name = Column(String(_name_length), nullable=False)
    type = Column(ChoiceType([(x, x) for x in TYPES]))
    resource_id = Column(Integer)

    def set_type(self, type):
        self.type = type
        if not type in PhysicalResource.TYPES:
            PhysicalResource.TYPES.apppend(type)


class Asset(Base):
    TYPE_PROPERTY = 'property'  # land
    TYPES = (TYPE_PROPERTY)

    __tablename__ = 'assetTable'
    id = Column(Integer, primary_key=True)
    name = Column(String(_name_length), nullable=False)


class Allocation(Base):
    __tablename__ = 'allocationTable'
    id = Column(Integer, primary_key=True)
    name = Column(String(_name_length), nullable=False)


class Interval(Base):
    __tablename__ = 'intervalTable'
    id = Column(Integer, primary_key=True)
    name = Column(String(_name_length), nullable=False)


class Resource(Base):
    __tablename__ = 'resourceTable'
    id = Column(Integer, primary_key=True)
    name = Column(String(_name_length), nullable=False)


class HumanResource(Base):
    """ @amin """
    TYPE_PROGRAMMER = 'programmer'
    TYPE_ANALYST = 'analyst'
    TYPE_DESIGNER = 'designer'
    TYPE_PROJECT_MANAGER = 'project manager'

    TYPES = [TYPE_PROJECT_MANAGER,  TYPE_PROGRAMMER,
              TYPE_ANALYST,  TYPE_DESIGNER]

    def __init__(self, type=None):
        self.type = type

    __tablename__ = 'humanResourceTable'
    id = Column(Integer, primary_key=True)
    name = Column(String(_name_length), nullable=False)
    type = Column(ChoiceType([(x, x) for x in TYPES]))


class IndividualCollaborator(Base):
    __tablename__ = 'individualCollaboratorTable'
    id = Column(Integer, primary_key=True)
    name = Column(String(_name_length), nullable=False)


Database.get_instance().create_all()

Database.get_instance().add_transaction(System(name='Edu'))
Database.get_instance().add_transaction(System(name='Training'))
Database.get_instance().add_transaction(System(name='Culture'))

print(ModuleCatalog.getInstance().addModule(
    Module(name='Courses', usage='Show all courses', system_id=1)))
