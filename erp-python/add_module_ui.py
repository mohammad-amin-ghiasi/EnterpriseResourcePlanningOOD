from tkinter import *
from tkinter.ttk import *
from requirement_manager_ui import BaseFrame
from components import *

class AddModuleUI(BaseFrame):
    def __init__(self, master, system_catalog, module_catalog):
        super().__init__(master)
        self.system_catalog = system_catalog
        self.module_catalog = module_catalog
        self.add_widgets()
        self.add_accept()
        self.accept_button.pack(side="left")

    def add_widgets(self):
        self.system_choice_widget = self.combo(self.get_system_choices(), frame=self, pack=True, side="left")
        self.name_var = StringVar()
        Label(self, text="name").pack(side="left")
        Entry(self, textvariable=self.name_var).pack(side="left")
        Label(self, text="usage").pack(side="left")
        self.usage_var = StringVar()
        Entry(self, textvariable=self.usage_var).pack(side="left")

    def get_system(self):
        return self.systems[self.system_choice_widget.current()]
    def get_name(self):
        return self.name_var.get()
    def get_usage(self):
        return self.usage_var.get()

    def get_system_choices(self):
        self.systems = self.system_catalog.find_all()
        return ["{{ {}, {} }}".format(m.get_id(), m.get_name()) for m in self.systems ]

    def hydrate(self):
        return Module(system=self.get_system(), usage=self.get_usage(), name=self.get_name())

    def accept(self):
        module = self.hydrate()
        self.module_catalog.addModule(module)
        self.master.destroy()
        print("module\n{}\nadded successfully".format(module))
        

if __name__ == '__main__':

    root = Tk()
    f = AddModuleUI(root, SystemCatalog.getInstance(), ModuleCatalog.getInstance())
    f.pack()
    root.mainloop()

