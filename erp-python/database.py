from sqlalchemy.engine import create_engine
from sqlalchemy.ext.declarative.api import declarative_base
from sqlalchemy.orm.session import sessionmaker


class MyClass(object):
    @staticmethod
    def the_static_method(x):
        print(x)


class Database:
    name = 'erp'
    uri = 'sqlite:///{name}.sqlite'.format(name=name)
    _instance = None
    _base_instance = None
    _engine_instance = None
    _session_instance = None

    @staticmethod
    def get_instance():
        """
        :rtype: Database
        """
        if Database._instance is None:
            Database._instance = Database()
        return Database._instance

    def get_engine(self):
        if self._engine_instance is None:
            self._base_instance = create_engine(self.uri)
        return self._base_instance

    def get_base(self):
        if self._base_instance is None:
            self._base_instance = declarative_base()
        return self._base_instance

    def create_all(self):
        return self.get_base().metadata.create_all(self.get_engine())

    @property
    def _get_session_class(self):
        return sessionmaker(bind=self.get_engine())

    def get_session(self):
        if self._session_instance is None:
            self._session_instance = self._get_session_class()
        return self._session_instance

    def add_transaction(self, save_object):
        session = self.get_session()
        session.add(save_object)
        session.commit()
