class Requirement:
    def __init__(self):
        super().__init__()

class InformationResourceRequirement(Requirement):
    def __init__(self, type, id):
        self.type = type
        self.id = id


class PhysicalResourceRequirement(Requirement):
    def __init__(self, type, count):
        self.type = type
        self.count = count

    def set_type(self, type):
        self.type = type
        self.count = count

class HumanResourceRequirement(Requirement):
    def __init__(self, type, count):
        self.type = type 
        self.count = count

    def set_type(self, type):
        self.type = type
    
    def set_count(self, count):
        self.count = count

    def __str__(self):
        return "{} {}".format(self.humanResourceType, self.count)
