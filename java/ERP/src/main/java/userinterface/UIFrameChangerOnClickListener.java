package userinterface;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by jun on 5/25/16.
 * A simple LyAndroid Code.
 */
public class UIFrameChangerOnClickListener implements ActionListener {
    private final JFrame frame;
    private final ReplaceableForm replaceableForm;

    public UIFrameChangerOnClickListener(JFrame frame, ReplaceableForm replaceableForm) {
        this.frame = frame;
        this.replaceableForm = replaceableForm;
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        frame.setContentPane(replaceableForm.getView());
        frame.pack();
    }
}
