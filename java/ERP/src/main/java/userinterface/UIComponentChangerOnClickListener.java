package userinterface;

import userinterface.forms.replacer.PanelContentReplacer;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by jun on 5/25/16.
 * A simple LyAndroid Code.
 */
public class UIComponentChangerOnClickListener implements ActionListener {
    private final JPanel jPanel;
    private final ReplaceableForm replaceableForm;

    public UIComponentChangerOnClickListener(JPanel basePanel, ReplaceableForm replaceableForm) {
        this.jPanel = basePanel;
        this.replaceableForm = replaceableForm;
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        PanelContentReplacer.getInstance().replaceView(jPanel, replaceableForm);
    }

}
