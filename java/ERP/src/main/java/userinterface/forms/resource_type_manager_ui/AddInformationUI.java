package userinterface.forms.resource_type_manager_ui;

import userinterface.ReplaceableForm;

import javax.swing.*;

/**
 * Created by jun on 8/15/16.
 * A simple LyAndroid Code.
 */
public class AddInformationUI implements ReplaceableForm {
    private final static AddInformationUI INSTANCE = new AddInformationUI();

    private AddInformationUI() {
    }

    public static AddInformationUI getInstance() {
        return INSTANCE;
    }

    private JPanel panel1;

    @Override
    public JPanel getView() {
        return panel1;
    }
}
