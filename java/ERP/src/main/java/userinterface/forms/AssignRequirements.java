package userinterface.forms;

import userinterface.ReplaceableForm;

import javax.swing.*;

/**
 * Created by jun on 5/25/16.
 * A simple LyAndroid Code.
 */
public class AssignRequirements implements ReplaceableForm {
    private final static AssignRequirements INSTANCE = new AssignRequirements();

    private AssignRequirements() {
    }

    public static AssignRequirements getInstance() {
        return INSTANCE;
    }

    private JPanel baseView;
    private JPanel contentPane;
    private JComboBox requirementType;
    private JTextField textField1;
    private JTextArea جزپیاتنیازمندیرااینجاTextArea;
    private JButton ok;
    private JButton افزودنوبستنButton;


    @Override
    public JPanel getView() {
        return baseView;
    }

    public JButton getOk() {
        return ok;
    }
}
