package userinterface.forms;

import database.MockedProjectsTable;
import userinterface.ReplaceableForm;

import javax.swing.*;

/**
 * Created by jun on 5/25/16.
 * A simple LyAndroid Code.
 */
public class MainPage implements ReplaceableForm {
    private final static MainPage INSTANCE = new MainPage();

    private MainPage() {
        this.setProjectsData();
    }

    public static MainPage getInstance() {
        return INSTANCE;
    }

    private JPanel baseView;
    private JTabbedPane mainTabs;
    private JPanel projects;
    private JPanel resources;
    private JPanel profile;
    private JButton newProjectButton;
    private JTable projectsTable;
    private JScrollPane tableScroll;
    private JPanel searchPanel;
    private JButton searchButton;
    private JTextField minDeveloper;
    private JTextField maxDeveloper;
    private JTextField projectName;
    private JLabel projectNameLabel;
    private JPanel developerPane;
    private JLabel maxDeveloperLabel;
    private JLabel minDeveloperLabel;
    private JPanel modulePane;
    private JPanel userPane;
    private JButton newResourceButton;


    private void setProjectsData() {
        this.projectsTable.setModel(new MockedProjectsTable());
    }

    @Override
    public JPanel getView() {
        return baseView;
    }

    public JButton getNewProjectButton() {
        return newProjectButton;
    }

    public JButton getNewResourceButton() {
        return newResourceButton;
    }

    //
//    public static void main(String[] args) {
//        JFrame tFrame = new JFrame();
//        MainPage main = new MainPage();
//        main.setProjectsData();
//        tFrame.setContentPane(main.getView());
//        tFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//        tFrame.pack();
//        tFrame.setVisible(true);
//    }

}
