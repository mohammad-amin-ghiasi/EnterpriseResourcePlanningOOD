package userinterface.forms.replacer;

import userinterface.ReplaceableForm;

import javax.swing.*;

/**
 * Created by jun on 8/15/16.
 * A simple LyAndroid Code.
 */
public class PanelContentReplacer {
    private final static PanelContentReplacer INSTANCE = new PanelContentReplacer();

    private PanelContentReplacer() {
    }

    public static PanelContentReplacer getInstance() {
        return INSTANCE;
    }


    public void replaceView(JPanel panel, ReplaceableForm replaceableForm) {
        panel.removeAll();
        System.out.println(replaceableForm.getView());
        System.out.println(panel);
        panel.add(replaceableForm.getView());
        panel.revalidate();
        panel.repaint();
    }
}
