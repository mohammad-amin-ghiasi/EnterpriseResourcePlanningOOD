package userinterface;

import javax.swing.*;

/**
 * Created by jun on 5/25/16.
 * A simple LyAndroid Code.
 */
public interface ReplaceableForm {
    public JPanel getView();
}
