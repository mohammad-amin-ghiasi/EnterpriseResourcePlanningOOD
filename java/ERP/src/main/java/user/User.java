package user;

import catalog.UserCatalog;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class User extends Collaborator {


    public String getName() {
        return name;
    }

    private String name;
    private String password;

    public User() {

    }

    public User(String name, String password) {
        this.name = name;
        this.password = password;
    }

    public static UserCatalog getCatalog() {
        return catalog;
    }

    static UserCatalog catalog = new UserCatalog();

}
