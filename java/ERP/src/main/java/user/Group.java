package user;

import catalog.Catalog;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "EGroup")
public class Group {
    @Id
    @GeneratedValue
    public int id;
    @OneToMany(cascade = CascadeType.ALL)
    Set<Collaborator> collaborators = new HashSet<>();

    public User addMember(User user) {
        collaborators.add(user);
        return user;
    }

    public static Catalog<Group> catalog = new Catalog<>(Group.class);
}
