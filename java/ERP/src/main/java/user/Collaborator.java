package user;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public abstract class Collaborator {
    @Id
    @GeneratedValue
    public int id;
}
