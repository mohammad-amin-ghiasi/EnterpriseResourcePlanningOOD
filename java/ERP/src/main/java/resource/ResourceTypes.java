package resource;

import userinterface.ReplaceableForm;
import userinterface.forms.resource_type_manager_ui.*;

/**
 * Created by jun on 8/15/16.
 * A simple LyAndroid Code.
 */
public enum ResourceTypes {
    CASH(AddCashUI.getInstance()),
    PHYSICAL(AddPhysicalUI.getInstance()),
    HUMAN(AddHumanUI.getInstance()),
    INFORMATION(AddInformationUI.getInstance()),;

    private final ReplaceableForm UIManager;

    ResourceTypes(ReplaceableForm uiManager) {
        UIManager = uiManager;
    }

    public ReplaceableForm getUIManager() {
        return UIManager;
    }
}
