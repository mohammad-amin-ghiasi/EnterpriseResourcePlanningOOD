package catalog;


import development.Module;

public class ModuleCatalog extends Catalog<Module> {
    public ModuleCatalog() {
        super(Module.class);
    }
}
