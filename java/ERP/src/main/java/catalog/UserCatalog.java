package catalog;


import user.User;

public class UserCatalog extends Catalog<User> {
    public UserCatalog() {
        super(User.class);
    }
}
