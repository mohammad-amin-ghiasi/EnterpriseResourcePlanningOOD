package catalog;

import db.DB;
import org.hibernate.Session;

import java.util.List;


public class Catalog<Entity> {

    private Class EntityType;

    public Catalog(Class entityType) {
        EntityType = entityType;
    }


    public Entity add(Entity entity) {
        Session session = DB.getInstance().getCurrentSession();
        session.beginTransaction();
        session.save(entity);
        session.getTransaction().commit();
        return entity;
    }

    public Entity update(Entity entity) {
        Session session = DB.getInstance().getCurrentSession();
        session.beginTransaction();
        session.update(entity);
        session.getTransaction().commit();
        return entity;
    }

    public Entity get(int id) {
        Session session = DB.getInstance().getCurrentSession();
        session.beginTransaction();
        Entity result = null;
        try {
            result = (Entity) session.get(EntityType, id);

        } catch (Exception e) {
            System.out.println("fetch from database failed");
        }
        session.getTransaction().commit();
        return result;
    }

    public List<Entity> list() {
        Session session = DB.getInstance().getCurrentSession();
        session.beginTransaction();
        //todo har chi gashtam peida nashod
        List result = session.createCriteria(EntityType).list();//gashtam harchi peida nashod ya kar nemidad
        session.getTransaction().commit();
        return result;
    }
}
