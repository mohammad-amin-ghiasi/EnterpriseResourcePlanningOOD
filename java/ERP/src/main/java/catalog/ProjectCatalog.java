package catalog;

import development.Project;

public class ProjectCatalog extends Catalog<Project> {
    public ProjectCatalog() {
        super(Project.class);
    }
}
