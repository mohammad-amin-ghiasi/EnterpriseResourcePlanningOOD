package catalog;


import development.Development;

public class DevelopmentCatalog extends Catalog<Development> {
    public DevelopmentCatalog() {
        super(Development.class);
    }
}
