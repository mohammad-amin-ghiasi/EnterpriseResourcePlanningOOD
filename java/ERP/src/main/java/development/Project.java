package development;

import catalog.ProjectCatalog;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Project {
    @Id
    @GeneratedValue
    private int id;
    private String name;
    private String description;

    public Project(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public Project() {
    }

    public static ProjectCatalog getCatalog() {
        return catalog;
    }

    private static ProjectCatalog catalog = new ProjectCatalog();
}
