package development;

import catalog.ModuleCatalog;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class Module extends Developed {


    public String getName() {
        return name;
    }

    private String name;

    public String getUsage() {
        return usage;
    }

    @Column(name = "EUsage")//usage is sql keyword;
    private String usage;

    public Module(String name, String usage) {
        this.name = name;
        this.usage = usage;
    }

    public Module() {
    }

    public static ModuleCatalog getCatalog() {
        return catalog;
    }

    private static ModuleCatalog catalog = new ModuleCatalog();

}
