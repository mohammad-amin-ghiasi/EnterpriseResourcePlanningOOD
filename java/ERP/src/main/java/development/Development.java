package development;

import catalog.Catalog;
import catalog.DevelopmentCatalog;

import javax.persistence.*;

@Entity
public class Development {
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Id
    @GeneratedValue
    private int id;
    private DevelopmentType type;
    private int period;//this time is set in hour
    @OneToOne(cascade = CascadeType.ALL)
    private DevelopmentTeam developmentTeam;
    private static DevelopmentCatalog catalog = new DevelopmentCatalog();
}

