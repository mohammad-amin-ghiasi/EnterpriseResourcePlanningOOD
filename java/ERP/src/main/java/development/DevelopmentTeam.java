package development;

import user.Group;
import user.User;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
public class DevelopmentTeam {
    @Id
    @GeneratedValue
    public int id;
    @OneToMany(cascade = CascadeType.ALL)
    public Set<Group> groups = new HashSet<>();
    @OneToMany(cascade = CascadeType.ALL)
    public Set<User> collaborators = new HashSet<>();
}
