package development;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class Developed {
    public void setId(int id) {
        this.id = id;
    }

    @Id
    @GeneratedValue
    private int id;


    public Set<Development> getDevelopments() {
        return developments;
    }


    @OneToMany(cascade = CascadeType.ALL)
    private Set<Development> developments = new HashSet<Development>();

    public Development addDevelopment(Development development) {
        developments.add(development);
        return development;
    }

}
